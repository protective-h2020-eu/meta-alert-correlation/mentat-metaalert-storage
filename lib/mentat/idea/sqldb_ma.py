#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------------
# This file is part of Mentat system (https://mentat.cesnet.cz/).
#
# Copyright (C) since 2011 CESNET, z.s.p.o (http://www.ces.net/)
# Use of this source is governed by the MIT license, see LICENSE file.
#-------------------------------------------------------------------------------


"""
This module provides class for object representation and conversion
of `IDEA <https://idea.cesnet.cz/en/index>`__  messages into their appropriate
`PostgreSQL <https://www.postgresql.org>`__ records. These records can be then
stored into database using the :py:mod:`mentat.services.eventstorage` event
persistent storage service.

The resulting record is intended to be stored into `PostgreSQL <https://www.postgresql.org>`__
database using **flat** schema. This is a very simple custom schema and it was
designed to tackle performance issues with full relational schema representation
of `IDEA <https://idea.cesnet.cz/en/index>`__ messages. It is basically a single
database table with fixed set of prepared indexed columns for the purposes of
searching and the whole `IDEA <https://idea.cesnet.cz/en/index>`__ message is then
stored as `PostgreSQL's <https://www.postgresql.org>`__ native ``jsonb``
datatype inside the last table column.

This module is expected to work only with messages based on or compatible with the
:py:class:`mentat.idea.internal.Idea` class.

This module contains following message class:

* :py:class:`mentat.idea.sqldb.Idea`

    Forward conversion into PostgreSQL data format.
"""

import psycopg2

class IPList(list):
    """
    Custom list container for :py:mod:`ipranges` objects. This was implemented
    in order to support custom adaptation of IP objects into SQL query. Please
    see the :py:class:`mentat.services.eventstorage.IPListAdapter` for more
    details. Please see the `psycopg2 documentation <http://initd.org/psycopg/docs/advanced.html#adapting-new-python-types-to-sql-syntax>`__
    for in-depth explanation.
    """
    pass

class Idea:    # pylint: disable=locally-disabled,too-many-instance-attributes,too-few-public-methods
    """
    Performs conversion of IDEA messages into flat relational model.
    """
    ident                       = None
    detecttime                  = None
    category                    = list()
    email                       = list()
    maquality                   = None
    rule                        = None
    topassetcriticality         = None
    maxcvss                     = None
    aggrid                      = list()
    source_ip                   = IPList()
    source_port                 = list()
    source_type                 = set()
    source_maxassetcriticality  = None
    source_hostname             = list()
    target_ip                   = IPList()
    target_port                 = list()
    target_type                 = set()
    target_maxassetcriticality  = None
    target_hostname             = list()
    protocol                    = set()
    jsonb                       = None

    def __init__(self, idea_event):
        """
        :param mentat.idea.internal.Idea idea_event: IDEA event object.
        """
        # Convert the whole IDEA event object to JSON string and then encode it
        # to bytes. The event will be stored as a BYTEA datatype within the
        # PostgreSQL database istead of JSONB, because PostgreSQL is unable to
        # store JSON objects that contain null characters anywhere in the content.
        self.jsonb = psycopg2.Binary(idea_event.to_json().encode('utf-8'))

        self.ident       = idea_event['ID']
        self.detecttime  = idea_event['DetectTime']
        self.category    = list(idea_event.get("Category", list()))
        self.email    = list(idea_event.get("Email", list()))
        self.rule = idea_event.get('Rule', None)
        self.aggrid = list(idea_event.get('AggrID', list()))
        self.topassetcriticality = idea_event.get('TopAssetCriticality', None)
        self.maxcvss = idea_event.get('maxCvss', None)
        # MA quality
        self.maquality = idea_event.get('MAQuality', None)

        # Source/Target Asset Criticality
        self.source_maxassetcriticality = self._get_criticality(idea_event, 'Source', 'MaxAssetCriticality')
        self.target_maxassetcriticality = self._get_criticality(idea_event, 'Target', 'MaxAssetCriticality')
        
        # Source IP (both v4 a v6 in single attribute).
        self.source_ip = IPList()
        item_list = self._get_subitems(idea_event, 'Source', 'IP4')
        self.source_ip.extend([ip for ip in item_list])
        item_list = self._get_subitems(idea_event, 'Source', 'IP6')
        self.source_ip.extend([ip for ip in item_list])

        # Target IP (both v4 a v6 in single attribute).
        self.target_ip = IPList()
        item_list = self._get_subitems(idea_event, 'Target', 'IP4')
        self.target_ip.extend([ip for ip in item_list])
        item_list = self._get_subitems(idea_event, 'Target', 'IP6')
        self.target_ip.extend([ip for ip in item_list])

        # Types (tags).
        self.source_type = sorted(list(set(self._get_subitems(idea_event, 'Source', 'Type'))))
        self.target_type = sorted(list(set(self._get_subitems(idea_event, 'Target', 'Type'))))

        # Ports.
        self.source_port = sorted(list(set(self._get_subitems(idea_event, 'Source', 'Port'))))
        self.target_port = sorted(list(set(self._get_subitems(idea_event, 'Target', 'Port'))))

        # HostNames.
        self.source_hostname = sorted(list(set(self._get_subitems(idea_event, 'Source', 'Hostname'))))
        self.target_hostname = sorted(list(set(self._get_subitems(idea_event, 'Target', 'Hostname'))))

        # Protocol (both source and target in single attribute).
        self.protocol = set()
        source_proto = self._get_subitems(idea_event, 'Source', 'Proto')
        for item in source_proto:
            self.protocol.add(item.lower())
        target_proto = self._get_subitems(idea_event, 'Target', 'Proto')
        for item in target_proto:
            self.protocol.add(item.lower())
        self.protocol = sorted(list(self.protocol))

    @staticmethod
    def _get_criticality(obj, key, subkey):
        """
        Helper method for merging and retrieving lists from complex IDEA message
        substructures (lists within dicts within lists within dict). As an example
        usage this method can be used for obtaining all IP4 addresses within all
        sources in IDEA message.
        """
        result = None
        try:
            result = obj[key][0][subkey]
        except KeyError:
            pass
        return result

    @staticmethod
    def _get_subitems(obj, key, subkey):
        """
        Helper method for merging and retrieving lists from complex IDEA message
        substructures (lists within dicts within lists within dict). As an example
        usage this method can be used for obtaining all IP4 addresses within all
        sources in IDEA message.
        """
        result = []
        try:
            for item in obj[key]:
                try:
                    result.extend(item[subkey])
                except KeyError:
                    pass
        except KeyError:
            pass
        return result

    def get_record(self):
        """
        Return tuple containing object attributes in correct order for insertion
        into PostgreSQL database using the :py:mod:`mentat.services.eventstorage`
        service.
        """
        return (
            self.ident,
            self.detecttime,
            self.category,
            self.email,
            self.maquality,
            self.rule,
            self.topassetcriticality,
            self.maxcvss,
            self.aggrid,
            self.source_ip,
            self.source_port,
            self.source_type,
            self.source_maxassetcriticality,
            self.source_hostname,
            self.target_ip,
            self.target_port,
            self.target_type,
            self.target_maxassetcriticality,
            self.target_hostname,
            self.protocol,
            self.jsonb
        )
