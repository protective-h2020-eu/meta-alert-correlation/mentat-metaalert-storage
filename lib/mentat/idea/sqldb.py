#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------------
# This file is part of Mentat system (https://mentat.cesnet.cz/).
#
# Copyright (C) since 2011 CESNET, z.s.p.o (http://www.ces.net/)
# Use of this source is governed by the MIT license, see LICENSE file.
#-------------------------------------------------------------------------------


"""
This module provides class for object representation and conversion
of `IDEA <https://idea.cesnet.cz/en/index>`__  messages into their appropriate
`PostgreSQL <https://www.postgresql.org>`__ records. These records can be then
stored into database using the :py:mod:`mentat.services.eventstorage` event
persistent storage service.

The resulting record is intended to be stored into `PostgreSQL <https://www.postgresql.org>`__
database using **flat** schema. This is a very simple custom schema and it was
designed to tackle performance issues with full relational schema representation
of `IDEA <https://idea.cesnet.cz/en/index>`__ messages. It is basically a single
database table with fixed set of prepared indexed columns for the purposes of
searching and the whole `IDEA <https://idea.cesnet.cz/en/index>`__ message is then
stored as `PostgreSQL's <https://www.postgresql.org>`__ native ``jsonb``
datatype inside the last table column.

The schema currently supports indexing of following `IDEA <https://idea.cesnet.cz/en/index>`__
message attributes:

* ID
* DetectTime
* Category
* Description
* Source.IP (both v4 and v6)
* Source.Port
* Source.Type
* Target.IP (both v4 and v6)
* Target.Port
* Target.Type
* Protocol (both source and target, unique set)
* Node.Name
* Node.Type
* _CESNET.ResolvedAbuses
* _CESNET.StorageTime

As a side-effect of this approach, searching according to other IDEA message attributes
is not possible.


This module is expected to work only with messages based on or compatible with the
:py:class:`mentat.idea.internal.Idea` class.


This module contains following message class:

* :py:class:`mentat.idea.sqldb.Idea`

    Forward conversion into PostgreSQL data format.


Example usage:

.. code-block:: python

    >>> import mentat.idea.internal
    >>> import mentat.idea.sqldb

    # IDEA messages ussually come from regular dicts or JSON.
    >>> idea_raw = {...}

    # Just pass the dict as parameter to constructor to create internal IDEA.
    >>> idea_msg = mentat.idea.internal.Idea(idea_raw)

    # Just pass the IDEA message as parameter to constructor to create SQL record.
    >>> idea_postgresql = mentat.idea.sqldb.Idea(idea_msg)
"""


__author__ = "Jan Mach <jan.mach@cesnet.cz>"
__credits__ = "Radko Krkoš <radko.krkos@cesnet.cz>, Pavel Kácha <pavel.kacha@cesnet.cz>, Andrea Kropáčová <andrea.kropacova@cesnet.cz>"


import psycopg2
import json

class IPList(list):
    """
    Custom list container for :py:mod:`ipranges` objects. This was implemented
    in order to support custom adaptation of IP objects into SQL query. Please
    see the :py:class:`mentat.services.eventstorage.IPListAdapter` for more
    details. Please see the `psycopg2 documentation <http://initd.org/psycopg/docs/advanced.html#adapting-new-python-types-to-sql-syntax>`__
    for in-depth explanation.
    """
    pass

class Idea:    # pylint: disable=locally-disabled,too-many-instance-attributes,too-few-public-methods
    """
    Performs conversion of IDEA messages into flat relational model.
    """
    ident                   = None
    detecttime              = None
    category                = list()
    description             = None
    source_ip               = IPList()
    target_ip               = IPList()
    source_port             = list()
    target_port             = list()
    source_type             = set()
    target_type             = set()
    protocol                = set()
    node_name               = list()
    node_type               = set()
    node_software           = set()
    email                   = set()
    cesnet_resolvedabuses   = list()
    cesnet_storagetime      = None
    cesnet_eventclass       = None
    cesnet_eventseverity    = None
    cesnet_inspectionerrors = list()
    passive_dns             = None
    entity_reputation       = None
    score                   = None
    completness             = None
    alert_freshness         = None
    source_relevance        = None
    quality                 = None
    certainty               = None
    source_trustworthiness  = None
    ip_recurrence           = None
    jsonb                   = None

    def __init__(self, idea_event):
        """
        :param mentat.idea.internal.Idea idea_event: IDEA event object.
        """
        # Convert the whole IDEA event object to JSON string and then encode it
        # to bytes. The event will be stored as a BYTEA datatype within the
        # PostgreSQL database istead of JSONB, because PostgreSQL is unable to
        # store JSON objects that contain null characters anywhere in the content.
        self.jsonb = psycopg2.Binary(idea_event.to_json().encode('utf-8'))

        self.ident       = idea_event['ID']
        self.detecttime  = idea_event['DetectTime']
        self.category    = list(idea_event.get("Category", list()))
        self.description = idea_event.get('Description', None)

        # Source IP (both v4 a v6 in single attribute).
        self.source_ip = IPList()
        item_list = self._get_subitems(idea_event, 'Source', 'IP4')
        self.source_ip.extend([ip for ip in item_list])
        item_list = self._get_subitems(idea_event, 'Source', 'IP6')
        self.source_ip.extend([ip for ip in item_list])

        # Target IP (both v4 a v6 in single attribute).
        self.target_ip = IPList()
        item_list = self._get_subitems(idea_event, 'Target', 'IP4')
        self.target_ip.extend([ip for ip in item_list])
        item_list = self._get_subitems(idea_event, 'Target', 'IP6')
        self.target_ip.extend([ip for ip in item_list])

        # Types (tags).
        self.source_type = sorted(list(set(self._get_subitems(idea_event, 'Source', 'Type'))))
        self.target_type = sorted(list(set(self._get_subitems(idea_event, 'Target', 'Type'))))

        # Ports.
        self.source_port = sorted(list(set(self._get_subitems(idea_event, 'Source', 'Port'))))
        self.target_port = sorted(list(set(self._get_subitems(idea_event, 'Target', 'Port'))))

        # Protocol (both source and target in single attribute).
        self.protocol = set()
        source_proto = self._get_subitems(idea_event, 'Source', 'Proto')
        for item in source_proto:
            self.protocol.add(item.lower())
        target_proto = self._get_subitems(idea_event, 'Target', 'Proto')
        for item in target_proto:
            self.protocol.add(item.lower())
        self.protocol = sorted(list(self.protocol))

        # Node fields
        self.node_name = [node["Name"].lower() for node in idea_event["Node"]]
        self.node_type = sorted(list(set(self._get_subitems(idea_event, 'Node', 'Type'))))
        self.node_software = sorted(list(set(self._get_subitems(idea_event, 'Node', 'SW'))))
        self.email = sorted(list(set(self._get_subitems(idea_event, 'Source', 'Email'))))

        # Enrichment fields
        self.passive_dns  = json.dumps(next(iter(idea_event.get('Enrich', []) or []), None))
        self.alert_quality  = json.dumps(idea_event.get('AlertQuality', None))
        self.entity_reputation = idea_event.get('EntityReputation', None)

        # Alert quality fields
        self.score                  = idea_event.get('AlertQuality', {}).get("Score", None)
        self.completness            = idea_event.get('AlertQuality', {}).get("Inputs", {}).get("Completeness", None)
        self.alert_freshness        = idea_event.get('AlertQuality', {}).get("Inputs", {}).get("AlertFreshness", None)
        self.source_relevance       = idea_event.get('AlertQuality', {}).get("Inputs", {}).get("SourceRelevance", None)
        self.quality                = idea_event.get('AlertQuality', {}).get("Opinion", {}).get("Quality", None)
        self.certainty              = idea_event.get('AlertQuality', {}).get("Opinion", {}).get("Certainty", None)
        self.source_trustworthiness = idea_event.get('AlertQuality', {}).get("Opinion", {}).get("SourceTrustworthiness", None)
        self.ip_recurrence          = idea_event.get('AlertQuality', {}).get("IpRecurrence", None)

        # CESNET custom fields
        self.cesnet_resolvedabuses   = list(idea_event.get('_CESNET', {}).get("ResolvedAbuses", list()))
        self.cesnet_storagetime      = idea_event['_CESNET']['StorageTime']
        self.cesnet_eventclass       = idea_event.get('_CESNET', {}).get('EventClass', None)
        self.cesnet_eventseverity    = idea_event.get('_CESNET', {}).get('EventSeverity', None)
        self.cesnet_inspectionerrors = list(idea_event.get('_CESNET', {}).get("InspectionErrors", list()))
        if self.cesnet_eventclass:
            self.cesnet_eventclass = self.cesnet_eventclass.lower()
        if self.cesnet_eventseverity:
            self.cesnet_eventseverity = self.cesnet_eventseverity.lower()

        ##
        ## After migrating from MongoDB to PostgreSQL and deprecating MongoDB
        ## following code may be uncommented and replace the lines above.
        ##
        #self.jsonb       = psycopg2.Binary(idea_event.to_json().encode('utf-8'))
        #self.ident       = idea_event.get_id()
        #self.detecttime  = idea_event.get_detect_time()
        #self.category    = idea_event.get_categories()
        #self.description = idea_event.get_description()

        ## Source IP (both v4 a v6 in single attribute).
        #self.source_ip = IPList()
        #self.source_ip.extend([ip for ip in idea_event.get_addresses('Source')])

        ## Target IP (both v4 a v6 in single attribute).
        #self.target_ip = IPList()
        #self.target_ip.extend([ip for ip in idea_event.get_addresses('Target')])

        ## Ports.
        #self.source_port = idea_event.get_ports('Source')
        #self.target_port = idea_event.get_ports('Target')

        ## Types (tags).
        #self.source_type = idea_event.get_types('Source')
        #self.target_type = idea_event.get_types('Target')

        ## Protocol (both source and target in single attribute).
        #source_proto = idea_event.get_protocols('Source')
        #target_proto = idea_event.get_protocols('Target')
        #self.protocol = sorted(list(set(source_proto + target_proto)))

        #self.node_name = idea_event.get_detectors()
        #self.node_type = idea_event.get_types('Node')

        #self.cesnet_resolvedabuses = idea_event.get_abuses()
        #self.cesnet_storagetime    = idea_event.get_storage_time()

    @staticmethod
    def _get_subitems(obj, key, subkey):
        """
        Helper method for merging and retrieving lists from complex IDEA message
        substructures (lists within dicts within lists within dict). As an example
        usage this method can be used for obtaining all IP4 addresses within all
        sources in IDEA message.
        """
        result = []
        try:
            for item in obj[key]:
                try:
                    result.extend(item[subkey])
                except KeyError:
                    pass
        except KeyError:
            pass
        return result

    def get_record(self):
        """
        Return tuple containing object attributes in correct order for insertion
        into PostgreSQL database using the :py:mod:`mentat.services.eventstorage`
        service.
        """
        return (
            self.ident,
            self.detecttime,
            self.category,
            self.description,
            self.source_ip,
            self.target_ip,
            self.source_port,
            self.target_port,
            self.source_type,
            self.target_type,
            self.protocol,
            self.node_name,
            self.node_type,
            self.node_software,
            self.email,
            self.cesnet_resolvedabuses,
            self.cesnet_storagetime,
            self.cesnet_eventclass,
            self.cesnet_eventseverity,
            self.cesnet_inspectionerrors,
            self.passive_dns,
            self.entity_reputation,
            self.score,
            self.completness,
            self.alert_freshness,
            self.source_relevance,
            self.quality,
            self.certainty,
            self.source_trustworthiness,
            self.ip_recurrence,
            self.jsonb
        )
