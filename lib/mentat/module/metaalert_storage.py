#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# Custom libraries.
#
import mentat.const
import mentat.daemon.piper
import mentat.daemon.component.parser
import mentat.daemon.component.metaalert_storage
import mentat.daemon.component.commiter


class MentatMetaAlertStorageDaemon(mentat.daemon.piper.PiperDaemon):

    def __init__(self):
        """
        Initialize storage daemon object. This method overrides the base
        implementation in :py:func:`pyzenkit.zendaemon.ZenDaemon.__init__` and
        it aims to even more simplify the daemon object creation by providing
        configuration values for parent contructor.
        """
        super().__init__(

            description = 'mentat-metaalert-storage.py - Meta-alert message storing daemon',

            #
            # Configure required deamon paths.
            #
            path_bin = '/usr/local/bin',
            path_cfg = '/etc/mentat',
            path_log = '/var/mentat/log',
            path_run = '/var/mentat/run',
            path_tmp = '/var/tmp',

            #
            # Override default configurations.
            #
            default_config_dir     = '/etc/mentat',
            default_queue_in_dir   = '/var/mentat/spool/mentat-metaalert-storage.py',
            default_queue_out_dir  = None,
            default_stats_interval  = mentat.const.DFLT_INTERVAL_STATISTICS,
            default_runlog_interval = mentat.const.DFLT_INTERVAL_RUNLOG,

            #
            # Schedule initial events.
            #
            schedule = [
                (mentat.const.DFLT_EVENT_START,)
            ],
            schedule_after = [
                (mentat.const.DFLT_INTERVAL_STATISTICS, mentat.const.DFLT_EVENT_LOG_STATISTICS),
                (mentat.const.DFLT_INTERVAL_RUNLOG,     mentat.const.DFLT_EVENT_SAVE_RUNLOG)
            ],

            #
            # Define required daemon components.
            #
            components = [
                mentat.daemon.component.parser.ParserDaemonComponent(),
                mentat.daemon.component.metaalert_storage.MetaAlertStorageDaemonComponent(),
                mentat.daemon.component.commiter.CommiterDaemonComponent()
            ]
        )