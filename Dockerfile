FROM scratch

COPY lib /usr/local/bin/
COPY bin /usr/local/bin/
COPY conf /etc/mentat/
