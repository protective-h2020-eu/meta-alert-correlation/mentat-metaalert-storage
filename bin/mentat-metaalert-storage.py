#!/usr/bin/env python3
# -*- coding: utf-8 -*-


from mentat.module.metaalert_storage import MentatMetaAlertStorageDaemon


if __name__ == "__main__":

    MentatMetaAlertStorageDaemon().run()
